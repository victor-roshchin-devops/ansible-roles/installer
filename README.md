# Роль для установки пакетов

**Список:**
1. Docker 
2. Kubectl 
3. Minikube 
4. Другие пакеты (опционально)


**Стандартные параметры для переназначения содержится в переменной `to_install` в виде массива:**

1. Включение и отключение установки docker
        
        to_install:
          ...
          docker:
            enabled: False              # Установить docker
            docker_compose:             # Установить docker-compose и указать версию при необходимости
              enabled: False
            add_users:                  # Список пользователей, которые будут добавлены в группу docker

2. Включение и отключение установки kubectl
        
        to_install:
          ...
          kubectl:
            enabled: False              # Установить kubectl
            version: "1.28.4"           # Версия kubectl

3. Включение и отключение установки minikube

        to_install:
          ...
          minikube:
            enabled: False              # Установить minikube
            version: "latest"           # Версия minikube
4. Список пакетов для установки

        to_install:
          ...
          packages: []
          